plugins {
    id("net.thesilkminer.tooling.cf-tool-conventions")
    id("net.thesilkminer.tooling.cf-tool-shadow-conventions")
}

@Suppress("PropertyName") val `groovy-version`: String by extra

dependencies {
    implementation(project(":core"))
    implementation(group = "org.codehaus.groovy", name = "groovy-cli-picocli", version = `groovy-version`, classifier = "indy")
}
