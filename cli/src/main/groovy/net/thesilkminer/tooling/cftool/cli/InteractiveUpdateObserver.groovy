/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.cli

import groovy.transform.PackageScope
import net.thesilkminer.tooling.cftool.core.ProcessStep
import net.thesilkminer.tooling.cftool.core.UpdateObserver

@PackageScope
final class InteractiveUpdateObserver implements UpdateObserver {
    private static final BAR_LENGTH = 25
    private static final STEP_LENGTH = ProcessStep.values().collect(BasicCliUtils.&makeDisplayNameFrom).collect(CharSequence.&length).max()
    private static final FILLED_MARKER = '='
    private static final EMPTY_MARKER = '-'

    private int currentlyFilled = 0
    private float fillingPerStep = BAR_LENGTH

    @Override
    void beginningSteps(final int quantity) {
        this.fillingPerStep = BAR_LENGTH / (quantity + 1)
    }

    @Override
    void beginStep(final ProcessStep step) {
        this.currentlyFilled += this.fillingPerStep
        this.printStep BasicCliUtils.makeDisplayNameFrom(step)
    }

    @Override
    void endStep(final ProcessStep step) {
        print '\r'
    }

    @Override
    void endStepExceptionally(final ProcessStep step, final Throwable exception) {
        println ' ERROR'
        println "An error occurred while processing step ${BasicCliUtils.makeDisplayNameFrom(step)}: $exception"
    }

    @Override
    void end() {
        this.currentlyFilled = BAR_LENGTH
        this.printStep ':pack'
        println ' DONE'
    }

    private printStep(final name) {
        print '<'
        for (int i = 0; i < this.currentlyFilled; ++i) print FILLED_MARKER
        for (int i = this.currentlyFilled; i < BAR_LENGTH; ++i) print EMPTY_MARKER
        print '> '
        print ((int) Math.floor(this.currentlyFilled * 100 / BAR_LENGTH))
        print "% $name"
        for (int i = 0; i < STEP_LENGTH - name.length(); ++i) print ' '
    }
}
