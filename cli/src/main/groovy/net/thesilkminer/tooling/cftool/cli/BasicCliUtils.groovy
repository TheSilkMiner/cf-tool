/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.cli

import groovy.transform.PackageScope
import net.thesilkminer.tooling.cftool.core.ProcessStep

import java.nio.file.Files
import java.nio.file.Path

@PackageScope
final class BasicCliUtils {
    private static final VALID_ZIP_HEADERS = [
            [0x50, 0x4B, 0x03, 0x04] as byte[],
            [0x50, 0x4B, 0x05, 0x06] as byte[],
            [0x50, 0x4B, 0x07, 0x08] as byte[],
    ]

    private static final PNG_HEADER = [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A] as byte[]

    static makeDisplayNameFrom(final ProcessStep step) {
        ":${makeNameFrom(step.name())}"
    }

    static makeFakeTaskFromMessage(final ProcessStep step, final String message) {
        ":${makeNameFrom(step.name())}:${makeNameFrom(message)}"
    }

    static makeNameFrom(final String name) {
        final splits = name.replace([' ' : '_']).toLowerCase(Locale.ENGLISH).split(/_/)
        splits[0] + splits.drop(1)*.capitalize().join('')
    }

    static isZip(final Path path) {
        Files.newInputStream(path).withCloseable {
            final bits = new byte[4]
            if (it.read(bits, 0, 4) < 4) {
                return false
            }

            VALID_ZIP_HEADERS.any { Arrays.equals bits, it }
        }
    }

    static isPng(final Path path) {
        Files.newInputStream(path).withCloseable {
            final bits = new byte[8]
            if (it.read(bits, 0, 8) < 8) {
                return false
            }

            bits == PNG_HEADER
        }
    }

    static isValidSizePng(final Path path) {
        return true // TODO
    }
}
