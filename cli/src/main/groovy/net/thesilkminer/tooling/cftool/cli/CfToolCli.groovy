/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.cli

import groovy.cli.picocli.CliBuilder
import net.thesilkminer.tooling.cftool.core.CfToolProcess

import java.nio.file.Files
import java.nio.file.Path

final class CfToolCli {
    static void main(final String... args) {
        final cli = new CliBuilder(name: 'cf-tool')
        cli.i(longOpt: 'input', args: 1, argName: 'pack-zip', required: true, type: Path, 'Curse Pack ZIP file, as downloaded from the website')
        cli.o(longOpt: 'output', args: 1, argName: 'instance-dir', required: true, type: Path, 'Directory where the instance will be created. Must be empty')
        cli.m(longOpt: 'minecraft', args: 1, argName: 'install-dir', type: Path, 'Location of the Minecraft install directory, for automatic profile and version discovery')
        cli.p(longOpt: 'profile-icon', args: 1, argName: 'png-image', type: Path, 'Location of a PNG image that will be used as the profile icon, if desired')
        cli._(longOpt: 'no-interactive', type: boolean, 'Disables interactive output')

        final options = cli.parse(args)
        if (!verify(options)) return

        new CfToolProcess(
                inputZip: options.i,
                mcInstallDirectory: options.m,
                profileIcon: options.p,
                outputDirectory: options.o,
                observer: options.'no-interactive'? new BasicUpdateObserver() : new InteractiveUpdateObserver()
        ).run()
    }

    // TODO("Improve checks")
    private static verify(final options) {
        options && verifyCompatibility(options) && verifyInput(options.i) && verifyOutput(options.o) && verifyMc(options.m) && verifyIcon(options.p)
    }

    private static verifyCompatibility(final options) {
        final i = options.i
        final o = options.o
        final m = options.m
        final p = options.p

        if (Files.isSameFile(i, o)) {
            printErr 'Input and output cannot point to the same file'
            return false
        }
        if (m && (Files.isSameFile(i, m) || Files.isSameFile(o, m))) {
            printErr 'Input or output cannot be located in the same place as the Minecraft install directory'
            return false
        }
        if (p && (Files.isSameFile(i, p) || Files.isSameFile(o, p))) {
            printErr 'Profile icon cannot be used as input or output'
            return false
        }

        return true
    }

    private static verifyInput(final i) {
        if (!Files.exists(i)) {
            printErr 'Input file does not exist'
            return false
        }
        if (Files.isDirectory(i)) {
            printErr 'Input file cannot be a directory'
            return false
        }
        if (!BasicCliUtils.isZip(i)) {
            printErr 'Input file is not a valid ZIP file'
            return false
        }
        return true
    }

    private static verifyOutput(final o) {
        if (!Files.exists(o)) {
            printErr 'Output directory does not exist'
            return false
        }
        if (!Files.isDirectory(o)) {
            printErr 'Output directory is not a directory'
            return false
        }
        return true
    }

    private static verifyMc(final m) {
        if (!m) return true
        final profiles = m.resolve('./launcher_profiles.json')
        final versions = m.resolve('./versions')

        if (!Files.exists(m)) {
            printErr 'Minecraft installation directory does not exist'
            return false
        }
        if (!Files.isDirectory(m)) {
            printErr 'Minecraft installation directory is not a directory'
            return false
        }
        if (!Files.exists(profiles) || !Files.isRegularFile(profiles)) {
            printErr 'Minecraft installation directory is not valid: no profiles found'
            return false
        }
        if (!Files.exists(versions) || !Files.isDirectory(versions)) {
            printErr 'Minecraft installation directory is not valid: no versions found'
            return false
        }
        return true
    }

    private static verifyIcon(final p) {
        if (!p) return true
        if (!Files.exists(p)) {
            printErr 'Profile icon does not exist'
            return false
        }
        if (Files.isDirectory(p)) {
            printErr 'Profile icon cannot be a directory'
            return false
        }
        if (!BasicCliUtils.isPng(p)) {
            printErr 'Profile icon must be a PNG image'
            return false
        }
        if (!BasicCliUtils.isValidSizePng(p)) {
            printErr 'Profile icon is not a PNG of a valid size'
            return false
        }
        return true
    }

    private static printErr(final CharSequence message) {
        System.err.println message
    }
}
