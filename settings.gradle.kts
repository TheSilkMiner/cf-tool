pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}

rootProject.name = "cf-tool"

include("core")
include("cli")
include("ui")
