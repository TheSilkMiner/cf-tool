/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import java.time.Instant
import java.util.concurrent.CompletableFuture

final class BuildProfileTask implements Task {
    @Override
    CompletableFuture<Void> executeTask(final TaskContext context) {
        TaskUtils.catchingFuture(context, 1) {
            context.observer.updateProcess(0, 'Writing Profile', 10)
            writeProfile(context)
        }
    }

    private static writeProfile(final context) {
        final uuid = UUID.randomUUID().toString()
        writeUuidToOutput(uuid, context.outputDirectory)
        if (context.mcInstallDirectory) {
            createProfile(uuid, context)
        }
    }

    private static writeUuidToOutput(final String uuid, final Path output) {
        Files.write(output.resolve('./.curseclient'), [ uuid ], StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)
    }

    private static createProfile(final String uuid, final TaskContext context) {
        createProfile(readMetadata(context, uuid), context)
    }

    private static createProfile(final metadata, final TaskContext context) {
        backupProfile(context.mcInstallDirectory)
        final path = context.mcInstallDirectory.resolve('./launcher_profiles.json')
        final launcherProfiles = new JsonSlurper().parse(path)
        final profiles = launcherProfiles.profiles
        profiles[metadata.uuid] = [
                created: Instant.now().toString(),
                gameDir: context.outputDirectory.toAbsolutePath().toString(),
                icon: context.profileIcon? makeIcon(context) : 'Redstone_Block',
                lastUsed: Instant.EPOCH.toString(),
                lastVersionId: metadata.version,
                name: metadata.name,
                type: 'custom'
        ]
        Files.writeString(path, new JsonBuilder(launcherProfiles).toPrettyString(), StandardCharsets.UTF_8, StandardOpenOption.WRITE)
    }

    private static backupProfile(final installDir) {
        final path = installDir.resolve('./launcher_profiles.json')
        final backupPath = installDir.resolve('./launcher_profiles.json.bak')
        Files.deleteIfExists(backupPath)
        Files.copy(path, backupPath)
    }

    private static readMetadata(final context, final uuid) {
        TaskUtils.makeZipFileSystem(context.inputZip) {
            final manifest = new JsonSlurper().parse(it.getPath('./manifest.json'))
            return [
                    uuid: uuid.replace([ '-' : '' ]),
                    name: manifest.name,
                    version: findVersion(manifest, context.mcInstallDirectory)
            ]
        }
    }

    private static findVersion(final manifest, final installDir) {
        if (!installDir) return

        final mcVersion = manifest.minecraft.version
        final modLoader = manifest.minecraft.modLoaders.find { it.primary }.id
        final versionPath = TaskUtils.findVersion(mcVersion, modLoader, installDir)
        versionPath.fileName.toString()
    }

    private static makeIcon(final context) {
        final image = Files.newInputStream(context.outputDirectory.resolve('./instance_small.png')).readAllBytes()
        final encodedImage = Base64.encoder.encodeToString(image)
        "data:image/png;base64,$encodedImage"
    }
}
