/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core

import net.thesilkminer.tooling.cftool.core.task.ApplyOverridesTask
import net.thesilkminer.tooling.cftool.core.task.BuildProfileTask
import net.thesilkminer.tooling.cftool.core.task.DownloadModFilesTask
import net.thesilkminer.tooling.cftool.core.task.PrepareWorkspaceTask
import net.thesilkminer.tooling.cftool.core.task.Task
import net.thesilkminer.tooling.cftool.core.task.TaskContext
import net.thesilkminer.tooling.cftool.core.task.VerifyInputDataTask
import net.thesilkminer.tooling.cftool.core.task.WriteMetadataTask

import java.util.concurrent.CompletableFuture

enum ProcessStep {
    VERIFY_INPUT_DATA(VerifyInputDataTask.&new),
    PREPARE_WORKSPACE(PrepareWorkspaceTask.&new),
    DOWNLOAD_MOD_FILES(DownloadModFilesTask.&new),
    APPLY_OVERRIDES(ApplyOverridesTask.&new),
    WRITE_METADATA(WriteMetadataTask.&new),
    BUILD_PROFILE(BuildProfileTask.&new)

    static final ProcessStep FIRST = VERIFY_INPUT_DATA
    static final ProcessStep LAST = BUILD_PROFILE

    final Closure<Task> taskCreator

    ProcessStep(final Closure<Task> taskCreator) {
        this.taskCreator = taskCreator
    }

    CompletableFuture<Void> executeTask(final CfToolProcess process) {
        process.observer.beginStep(this)
        (this.taskCreator() as Task).executeTask(TaskContext.wrap(process))
                .thenRun { process.observer.endStep(this) }
                .exceptionally {
                    process.observer.endStepExceptionally(this, it)
                    throw it
                }
    }
}
