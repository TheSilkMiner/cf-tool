/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.transform.PackageScope

import javax.imageio.ImageIO
import java.awt.Color
import java.awt.Image
import java.awt.image.BufferedImage
import java.nio.file.Files
import java.nio.file.Path

@PackageScope
final class ImageUtils {
    static byte[] resizeImage(final Path path) {
        final data = Files.newInputStream(path).readAllBytes()
        final image = ImageIO.read(new ByteArrayInputStream(data))
        final resizedImage = resizeImageIfRequired(image)

        if (resizedImage) {
            final os = new ByteArrayOutputStream()
            ImageIO.write(resizedImage, 'png', os)
            os.toByteArray()
        } else {
            data
        }
    }

    private static resizeImageIfRequired(final BufferedImage image) {
        image.width == 128 && image.height == 128? null : doResizeImage(image)
    }

    private static doResizeImage(final BufferedImage image) {
        final rescaledDimensions = rescale(width: image.width, height: image.height)
        final coordinates = [ x: (int) Math.floor((128 - rescaledDimensions.width) / 2), y: (int) Math.floor((128 - rescaledDimensions.height) / 2) ]
        final rescaledImage = image.getScaledInstance(rescaledDimensions.width, rescaledDimensions.height, Image.SCALE_SMOOTH)

        final finalImage = new BufferedImage(128, 128, image.type)
        final graphics = finalImage.createGraphics()
        final previousColor = graphics.getColor()
        graphics.setColor(Color.BLACK)
        graphics.fillRect(0, 0, 128, 128)
        graphics.setColor(previousColor)
        graphics.drawImage(rescaledImage, coordinates.x, coordinates.y, null)
        graphics.dispose()

        finalImage
    }

    private static rescale(final dimensions) {
        final rescaledDimensions = [ width: dimensions.width, height: dimensions.height ]

        if (rescaledDimensions.width > 128) {
            rescaledDimensions.width = 128
            rescaledDimensions.height = (int) Math.floor((rescaledDimensions.width * dimensions.height) / dimensions.width)
        }

        if (rescaledDimensions.height > 128) {
            rescaledDimensions.height = 128
            rescaledDimensions.width = (int) Math.floor((rescaledDimensions.height * dimensions.width) / dimensions.height)
        }

        rescaledDimensions
    }
}
