/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.transform.PackageScope
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.CompletableFuture

@PackageScope
final class TaskUtils {
    static CompletableFuture<Void> catchingFuture(final TaskContext context, final int fork, final Closure<?> closure) {
        context.observer.forkProcesses(fork)
        try {
            closure()
            CompletableFuture.completedFuture(null)
        } catch (e) {
            CompletableFuture.failedFuture(e)
        } finally {
            for (int i = 0; i < fork; ++i) context.observer.endProcess(i)
        }
    }

    static <T> T makeZipFileSystem(final Path zipFile, @ClosureParams(value = SimpleType, options = 'java.nio.file.FileSystem') final Closure<T> closure) {
        FileSystems.newFileSystem(zipFile).withCloseable(closure)
    }

    static resizeIcon(final Path path) {
        ImageUtils.resizeImage(path)
    }

    static findVersion(final String mcVersion, final String modLoader, final Path mcInstallDir) {
        if (!mcInstallDir) return null

        final versionsDir = mcInstallDir.resolve('./versions')
        final alternativeLoader = modLoader.capitalize().replace([ '-' : '' ])
        final possibleVersions = [
                modLoader,
                "$mcVersion-$modLoader",
                "$modLoader-$mcVersion",
                alternativeLoader,
                "$mcVersion-$alternativeLoader",
                "$alternativeLoader-$mcVersion"
        ]
        final versionPath = possibleVersions
                .collect { versionsDir.resolve("./$it") }
                .find { Files.exists(it) && Files.isDirectory(it) }

        if (!versionPath) {
            throw new IllegalArgumentException("No mod loader installed: looking for $modLoader for $mcVersion. Looked in $possibleVersions")
        }

        versionPath
    }
}
