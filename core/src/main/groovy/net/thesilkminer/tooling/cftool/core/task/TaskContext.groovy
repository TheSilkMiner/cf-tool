/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.transform.Immutable
import groovy.transform.Memoized
import net.thesilkminer.tooling.cftool.core.CfToolProcess

import java.nio.file.Path

@Immutable(knownImmutableClasses = [Path], knownImmutables = ['observer'])
final class TaskContext {
    Path inputZip
    Path mcInstallDirectory
    Path profileIcon
    Path outputDirectory
    TaskUpdateObserver observer

    @Memoized
    static wrap(final CfToolProcess process) {
        new TaskContext(
                inputZip: process.inputZip,
                mcInstallDirectory: process.mcInstallDirectory,
                profileIcon: process.profileIcon,
                outputDirectory: process.outputDirectory,
                observer: process.observer
        )
    }
}
