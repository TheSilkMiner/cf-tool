/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.json.JsonSlurper
import groovy.transform.Immutable

import java.nio.channels.Channels
import java.nio.channels.FileChannel
import java.nio.charset.StandardCharsets
import java.nio.file.StandardOpenOption
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executors

final class DownloadModFilesTask implements Task {
    @Immutable
    private static final class Mod {
        int project
        int file
    }

    private static final class DownloadingTask implements Runnable {
        private TaskContext context
        private Mod mod

        @Lazy private int id = { Thread.currentThread().name.split('-').last().toInteger() - 1 }()

        @Override
        void run() {
            try {
                final connection = retry(5) { buildConnection() }
                connection.connect()
                final dataStream = retry(5) { connection.inputStream }
                final fileName = findFileNameFrom(connection.URL) // Important: must be done after creating input stream. YAY side effects 🙄
                final maxLength = connection.contentLengthLong.with { it < 0? MAX_VALUE : it }

                retry(5) { this.doTransfer(fileName, dataStream, maxLength) }
            } catch (e) {
                throw new ExecutionException("Unable to download mod ${this.mod}", e)
            }
        }

        private buildConnection() {
            /*
            final url = "https://www.cursemaven.com/curse/maven/mod-${this.mod.project}/${this.mod.file}/mod-${this.mod.project}-${this.mod.file}.jar"
            new URI(url).toURL().openConnection()
            */
            final downloadUrlUrl = "https://addons-ecs.forgesvc.net/api/v2/addon/${this.mod.project}/file/${this.mod.file}/download-url"
            final downloadUrlConnection = new URI(downloadUrlUrl).toURL().openConnection()
            downloadUrlConnection.connect()
            final downloadUrlReader = new InputStreamReader(downloadUrlConnection.inputStream)
            final rawDownloadUrl = downloadUrlReader.readLines()[0]
            final splitDownloadUrl = rawDownloadUrl.split($///$)
            final encodedDownloadUrl = "${splitDownloadUrl.dropRight(1).join('/')}/${encodeUriComponent(splitDownloadUrl.last())}"
            try {
                new URI(encodedDownloadUrl).toURL().openConnection()
            } catch (e) {
                throw new IllegalStateException("Attempted to query invalid URL '$encodedDownloadUrl'", e)
            }
        }

        private static <T> T retry(final int times, final Closure<T> block) {
            if (times <= 0) return block()

            try {
                block()
            } catch (e) {
                try {
                    retry(times - 1, block) as T
                } catch (another) {
                    e.addSuppressed(another)
                    throw e
                }
            }
        }

        private static encodeUriComponent(final component) {
            URLEncoder.encode(component, StandardCharsets.UTF_8).replace([ '+' : '%20', '%21' : '!', '%27' : '\'', '%28' : '(', '%29' : ')', '%7E' : '~' ])
        }

        private static findFileNameFrom(final url) {
            URLDecoder.decode(url.path.split($///$).last().replace([ '%20' : '+' ]), StandardCharsets.UTF_8)
        }

        private doTransfer(final fileName, final dataStream, final length) {
            this.context.observer.updateProcess(this.id, "${fileName}", 0)

            ProgressTrackingChannel.of(Channels.newChannel(dataStream)) {
                this.context.observer.updateProcessStatus(this.id, (int) Math.floor(it * 100 / length))
            }.withCloseable { dataChannel ->
                final filePath = this.context.outputDirectory.resolve('./mods').resolve(fileName)
                FileChannel.open(filePath, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW).withCloseable { fileChannel ->
                    fileChannel.transferFrom(dataChannel, 0L, Long.MAX_VALUE)
                }
            }
        }
    }

    private final pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())

    @Override
    CompletableFuture<Void> executeTask(final TaskContext context) {
        context.observer.forkProcesses(Runtime.getRuntime().availableProcessors())
        for (int i = 1; i < Runtime.getRuntime().availableProcessors(); ++i) {
            context.observer.updateProcess(i, 'Idle', 0)
        }
        context.observer.updateProcess(0, 'Reading mod list', 0)

        final modList = findMods(context)
        CompletableFuture.allOf(this.makeFutures(context, modList))
                .thenRun { this.pool.shutdownNow() }
                .exceptionally {
                    this.pool.shutdownNow()
                    throw it
                }
    }

    private static findMods(final TaskContext context) {
        TaskUtils.makeZipFileSystem(context.inputZip) {
            final manifest = new JsonSlurper().parse(it.getPath('/manifest.json'))
            manifest.files.collect { new Mod(project: it.projectID, file: it.fileID) }
        }
    }

    private makeFutures(final TaskContext context, final modList) {
        modList.collect {
            CompletableFuture.runAsync(new DownloadingTask(context: context, mod: it), this.pool)
        }.toArray(CompletableFuture[].&new)
    }
}
