/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes
import java.util.concurrent.CompletableFuture

final class PrepareWorkspaceTask implements Task {
    private static final class WipeFileVisitor extends SimpleFileVisitor<Path> {
        @Override
        FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
            Files.delete(file)
            return FileVisitResult.CONTINUE
        }

        @Override
        FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
            if (exc) throw exc
            Files.delete(dir)
            return FileVisitResult.CONTINUE
        }
    }

    @Override
    CompletableFuture<Void> executeTask(final TaskContext context) {
        TaskUtils.catchingFuture(context, 1) {
            context.observer.updateProcess(0, 'Wiping output directory', 50)
            wipe(context.outputDirectory)

            context.observer.updateProcess(0, 'Creating output directory', 100)
            createDirectories(context.outputDirectory)
        }
    }

    private static wipe(final outputDirectory) {
        Files.walkFileTree(outputDirectory, new WipeFileVisitor())
    }

    private static createDirectories(final outputDirectory) {
        Files.createDirectories(outputDirectory)
        Files.createDirectory(outputDirectory.resolve('./mods'))
    }
}
