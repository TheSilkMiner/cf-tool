/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.json.JsonSlurper

import java.nio.channels.FileChannel
import java.nio.file.FileSystem
import java.nio.file.FileSystems
import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.StandardOpenOption
import java.nio.file.attribute.BasicFileAttributes
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executors

final class ApplyOverridesTask implements Task {
    private static final class OverridesDiscoverer extends SimpleFileVisitor<Path> {
        private List<Path> output
        private Path root

        @Override
        FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
            this.output << this.root.relativize(file)
            return FileVisitResult.CONTINUE
        }
    }

    private static final class CopyTask implements Runnable {
        private TaskContext context
        private Path overridesDirectory
        private Path relativePath

        @Lazy private int id = { Thread.currentThread().name.split('-').last().toInteger() - 1 }()

        @Override
        void run() {
            final name = this.relativePath.toString()
            final input = this.overridesDirectory.resolve(name)
            final output = this.context.outputDirectory.resolve(name)
            try {
                this.doTransfer(input, name, output)
            } catch (e) {
                throw new ExecutionException("Unable to apply override for ${this.relativePath} in $output", e)
            }
        }

        private void doTransfer(final input, final name, final output) {
            this.context.observer.updateProcess(this.id, name, 0)

            Files.deleteIfExists(output)
            Files.createDirectories(output.parent)

            Files.newByteChannel(input).withCloseable { dataChannel ->
                final length = dataChannel.size()
                ProgressTrackingChannel.of(dataChannel) {
                    this.context.observer.updateProcessStatus(this.id, (int) Math.floor(it * 100 / length))
                }.withCloseable { inputChannel ->
                    FileChannel.open(output, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW).withCloseable { outputChannel ->
                        outputChannel.transferFrom(inputChannel, 0L, Long.MAX_VALUE)
                    }
                }
            }
        }
    }

    private final pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())

    @Override
    CompletableFuture<Void> executeTask(final TaskContext context) {
        context.observer.forkProcesses(Runtime.getRuntime().availableProcessors())
        for (int i = 1; i < Runtime.getRuntime().availableProcessors(); ++i) {
            context.observer.updateProcess(i, 'Idle', 0)
        }
        context.observer.updateProcess(0, 'Discovering overrides', 0)

        final fs = FileSystems.newFileSystem(context.inputZip)
        final overridesDirectory = fs.getPath("./${findOverridesDirectory(fs)}").normalize()
        final overrides = discoverOverrides(overridesDirectory)
        CompletableFuture.allOf(this.makeFutures(context, overridesDirectory, overrides))
                .thenRun {
                    this.pool.shutdownNow()
                    fs.close()
                }
                .exceptionally {
                    this.pool.shutdownNow()
                    fs.close()
                    throw it
                }
    }

    private static discoverOverrides(final overridesDirectory) {
        final overrides = []
        Files.walkFileTree(overridesDirectory, new OverridesDiscoverer(output: overrides, root: overridesDirectory))
        overrides.reverse() as List<Path>
    }

    private static findOverridesDirectory(final FileSystem fs) {
        final manifest = new JsonSlurper().parse(fs.getPath('/manifest.json'))
        manifest.overrides
    }

    private makeFutures(final TaskContext context, final Path overridesDirectory, final List<Path> overrides) {
        overrides.collect {
            CompletableFuture.runAsync(new CopyTask(context: context, relativePath: it, overridesDirectory: overridesDirectory), this.pool)
        }.toArray(CompletableFuture[].&new)
    }
}
