/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.json.JsonSlurper

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.StandardOpenOption
import java.util.concurrent.CompletableFuture

final class WriteMetadataTask implements Task {

    @Override
    CompletableFuture<Void> executeTask(final TaskContext context) {
        TaskUtils.catchingFuture(context, 1) {
            context.observer.updateProcess(0, 'Reading Metadata', 25)
            final metadata = readMetadata(context)

            context.observer.updateProcess(0, 'Writing pack information', 50)
            writeAuthorVersionMetadata(context, metadata)

            context.observer.updateProcess(0, 'Drawing icons', 75)
            writeIcons(context, metadata)
        }
    }

    private static readMetadata(final context) {
        TaskUtils.makeZipFileSystem(context.inputZip) {
            final manifest = new JsonSlurper().parse(it.getPath('./manifest.json'))
            [
                    name: manifest.name,
                    version: manifest.version,
                    author: manifest.author,
                    bigIcon: context.profileIcon? Files.newInputStream(context.profileIcon).readAllBytes() : null,
                    icon: context.profileIcon? TaskUtils.resizeIcon(context.profileIcon) : null
            ]
        }
    }

    private static writeAuthorVersionMetadata(final context, final metadata) {
        final outFile = context.outputDirectory.resolve('./.cursepack')
        final data = [
                "version=${metadata.version}",
                "name=${metadata.name}",
                "author=${metadata.author}"
        ]
        Files.write(outFile, data, StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)
    }

    private static writeIcons(final context, final metadata) {
        if (metadata.bigIcon) {
            final bigIcon = context.outputDirectory.resolve('./instance.png')
            Files.write(bigIcon, metadata.bigIcon, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)
        }

        if (metadata.icon) {
            final icon = context.outputDirectory.resolve('./instance_small.png')
            Files.write(icon, metadata.icon, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)
        }
    }
}
