/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.json.JsonSlurper

import java.nio.file.Files
import java.util.concurrent.CompletableFuture

final class VerifyInputDataTask implements Task {
    private static final MANIFEST_VERIFICATION_TASKS = [
            VerifyInputDataTask.&verifyManifestVersion,
            VerifyInputDataTask.&verifyModLoaderExistence
    ]

    private static final PROFILE_VERIFICATION_TASKS = [
            VerifyInputDataTask.&verifyProfilesVersion
    ]

    private static final PERCENTAGE_PROGRESSION = MANIFEST_VERIFICATION_TASKS.size() + PROFILE_VERIFICATION_TASKS.size()

    @Override
    CompletableFuture<Void> executeTask(final TaskContext context) {
        TaskUtils.catchingFuture(context, 1) {
            final jsonParser = new JsonSlurper()
            TaskUtils.makeZipFileSystem(context.inputZip) {
                final manifest = jsonParser.parse(it.getPath('/manifest.json'))
                MANIFEST_VERIFICATION_TASKS.each { it(context, manifest) }
            }

            if (context.mcInstallDirectory) {
                final profiles = jsonParser.parse(context.mcInstallDirectory.resolve('./launcher_profiles.json'))
                PROFILE_VERIFICATION_TASKS.each { it(context, profiles) }
            }
        }
    }

    private static verifyManifestVersion(final TaskContext context, final manifest) {
        context.observer.updateProcess(0, 'Validating manifest', PERCENTAGE_PROGRESSION)

        final manifestType = manifest.manifestType
        final manifestVersion = manifest.manifestVersion
        if (manifestType != 'minecraftModpack' && manifestVersion != 1) {
            throw new IllegalArgumentException("Invalid pack ZIP file identified during processing: $manifestType@$manifestVersion is unknown")
        }
    }

    private static verifyModLoaderExistence(final TaskContext context, final manifest) {
        context.observer.updateProcess(0, 'Validating Profile version', 2 * PERCENTAGE_PROGRESSION)

        if (!context.mcInstallDirectory) return

        final mcVersion = manifest.minecraft.version
        final modLoader = manifest.minecraft.modLoaders.find { it.primary }.id
        TaskUtils.findVersion(mcVersion, modLoader, context.mcInstallDirectory)
    }

    private static verifyProfilesVersion(final TaskContext context, final profiles) {
        context.observer.updateProcess(0, 'Validating Minecraft profiles', 3 * PERCENTAGE_PROGRESSION)

        final version = profiles.version

        if (version != 3) {
            throw new IllegalArgumentException("Unknown profiles version $version")
        }
    }
}
