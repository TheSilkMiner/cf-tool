/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core.task

import groovy.transform.PackageScope
import groovy.transform.stc.ClosureParams
import groovy.transform.stc.SimpleType

import java.nio.ByteBuffer
import java.nio.channels.ReadableByteChannel

@PackageScope
final class ProgressTrackingChannel implements ReadableByteChannel {
    private final ReadableByteChannel channel
    private final Closure<?> listener

    private int totalRead

    private ProgressTrackingChannel(final ReadableByteChannel channel, final Closure<?> listener) {
        this.channel = channel
        this.listener = listener
    }

    @PackageScope
    static of(final ReadableByteChannel channel, @ClosureParams(value = SimpleType, options = 'java.lang.Integer') final Closure<?> listener) {
        return new ProgressTrackingChannel(channel, listener)
    }

    @Override
    int read(final ByteBuffer dst) throws IOException {
        final read = this.channel.read(dst)
        if (read > 0) {
            this.totalRead += read
            this.listener(this.totalRead)
        }
        return read
    }

    @Override
    boolean isOpen() {
        return this.channel.isOpen()
    }

    @Override
    void close() throws IOException {
        this.channel.close()
    }
}
