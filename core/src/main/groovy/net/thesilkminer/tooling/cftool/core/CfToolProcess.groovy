/*
 * CF-Tool: Download Manager and Profile Creator
 * Copyright (C) 2022  TheSilkMiner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.tooling.cftool.core

import groovy.transform.Immutable
import groovy.transform.TailRecursive

import java.nio.file.Path
import java.util.concurrent.ExecutionException

@Immutable(knownImmutableClasses = [Path], knownImmutables = ['observer'])
final class CfToolProcess implements Runnable {
    Path inputZip
    Path mcInstallDirectory
    Path profileIcon
    Path outputDirectory
    UpdateObserver observer

    @Override
    void run() {
        this.observer.beginningSteps(ProcessStep.values().length)

        final exception = runStep(ProcessStep.FIRST) as Throwable
        if (exception != null) {
            throw new RuntimeException(exception)
        }

        this.observer.end()
    }

    @TailRecursive
    private runStep(final ProcessStep step) {
        try {
            step.executeTask(this).get()
        } catch (final ExecutionException e) {
            return e
        }
        if (step == ProcessStep.LAST) return null
        runStep(step.next())
    }
}
