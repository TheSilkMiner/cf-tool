plugins {
    id("net.thesilkminer.tooling.cf-tool-conventions")
}

@Suppress("PropertyName") val `groovy-version`: String by extra

dependencies {
    implementation(group = "org.codehaus.groovy", name = "groovy-json", version = `groovy-version`, classifier = "indy")
}
