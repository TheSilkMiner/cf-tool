plugins {
    `groovy-gradle-plugin`
}

repositories {
    gradlePluginPortal()
    mavenCentral()
}

dependencies {
    gradleApi()
    implementation(group = "gradle.plugin.com.hierynomus.gradle.plugins", name = "license-gradle-plugin", version = "0.16.1")
    implementation(group = "gradle.plugin.com.github.johnrengelman", name = "shadow", version = "7.1.2")
}

gradlePlugin {
    plugins {
        create("cf-tool") {
            id = "net.thesilkminer.tooling.cf-tool-conventions"
            implementationClass = "net.thesilkminer.tooling.cftool.gradle.CfToolConventionsPlugin"
        }
        create("cf-tool-shadow"){
            id = "net.thesilkminer.tooling.cf-tool-shadow-conventions"
            implementationClass = "net.thesilkminer.tooling.cftool.gradle.CfToolShadowConventionsPlugin"
        }
    }
}
