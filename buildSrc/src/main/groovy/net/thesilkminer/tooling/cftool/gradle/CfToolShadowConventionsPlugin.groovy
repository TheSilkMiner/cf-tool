package net.thesilkminer.tooling.cftool.gradle

import com.github.jengelman.gradle.plugins.shadow.ShadowJavaPlugin
import com.github.jengelman.gradle.plugins.shadow.ShadowPlugin
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.Plugin
import org.gradle.api.Project

class CfToolShadowConventionsPlugin implements Plugin<Project> {
    @Override
    void apply(final Project target) {
        applyShadowPlugin target
    }

    private static applyShadowPlugin(final project) {
        project.plugins.apply ShadowPlugin
        project.plugins.apply ShadowJavaPlugin

        project.tasks.build.dependsOn project.tasks.shadowJar

        project.tasks.withType(ShadowJar).configureEach {
            
            it.exclude('META-INF/versions/**', 'META-INF/LICENSE', 'META-INF/NOTICE')
        }
    }
}
