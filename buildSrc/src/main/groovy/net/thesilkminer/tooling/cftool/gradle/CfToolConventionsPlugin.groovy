package net.thesilkminer.tooling.cftool.gradle

import nl.javadude.gradle.plugins.license.LicenseExtension
import nl.javadude.gradle.plugins.license.LicensePlugin
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.ExtraPropertiesExtension
import org.gradle.api.plugins.GroovyPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.jvm.toolchain.JavaLanguageVersion

class CfToolConventionsPlugin implements Plugin<Project> {
    @Override
    void apply(final Project target) {
        applyGroovy target
        setUpJavaToolchain target
        applyLicensing target
    }

    private static applyGroovy(final project) {
        project.plugins.apply GroovyPlugin
        final ext = project.extensions.getByType ExtraPropertiesExtension
        final groovyVersion = ext['groovy-version']

        project.repositories.add project.repositories.mavenCentral()
        [ 'groovy', 'groovy-nio', 'groovy-jsr223' ].each {
            final dep = project.dependencies.create group: 'org.codehaus.groovy', name: it, version: groovyVersion, classifier: 'indy'
            project.dependencies.add 'implementation', dep
        }

        project.tasks.withType(GroovyCompile).configureEach {
            groovyOptions.optimizationOptions.indy = true
        }
    }

    private static setUpJavaToolchain(final target) {
        final java = target.extensions.getByType JavaPluginExtension
        java.toolchain.languageVersion.set JavaLanguageVersion.of(JavaVersion.VERSION_17.majorVersion)
    }

    private static applyLicensing(final project) {
        project.plugins.apply LicensePlugin

        final licensingExtension = project.extensions.getByType LicenseExtension
        licensingExtension.header = project.rootProject.file 'NOTICE'
        licensingExtension.strictCheck = true
    }
}
